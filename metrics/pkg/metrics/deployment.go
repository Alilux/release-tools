package metrics

import (
	"net/http"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
)

var (
	deploymentDurationHistogram = promauto.NewHistogramVec(prometheus.HistogramOpts{
		Namespace: namespace,
		Subsystem: "deployment",
		Name:      "duration_seconds",
		Help:      "Duration of the coordinated deployment pipeline, from staging to production",
		Buckets:   prometheus.LinearBuckets(12_600, 30*60, 14), // 14 buckets of 30 minutes ranging from 3.5hrs to 10h.
	}, []string{"deployment_type", "status"})

	deploymentDurationGauge = promauto.NewGaugeVec(prometheus.GaugeOpts{
		Namespace: namespace,
		Subsystem: "deployment",
		Name:      "duration_last_seconds",
		Help:      "Duration of the latest coordinated deployment pipeline, from staging to production",
	}, []string{"deployment_type", "status"})
)

func deploymentDurationHistogramHandlerFunc(w http.ResponseWriter, r *http.Request) {
	duration, err := getValue(r)
	if err != nil {
		badRequest(w, r, "Missing or wrong value parameter")

		return
	}

	labels := getLabels(r)
	if len(labels) != 2 {
		badRequest(w, r, "Two labels expected")

		return
	}

	deploymentDurationHistogram.WithLabelValues(labels...).Observe(duration)

	answer(w, r, "New deployment recorded")
}

func deploymentDurationGaugeHandlerFunc(w http.ResponseWriter, r *http.Request) {
	duration, err := getValue(r)
	if err != nil {
		badRequest(w, r, "Missing or wrong value parameter")

		return
	}

	labels := getLabels(r)
	if len(labels) != 2 {
		badRequest(w, r, "Two labels expected")

		return
	}

	deploymentDurationGauge.WithLabelValues(labels...).Set(duration)

	answer(w, r, "New deployment recorded")
}
